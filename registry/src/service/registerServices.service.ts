import {serviceSchema} from "~/schemas/service";
import {database} from "~/config/database";

export default class RegisterServiceService {
    public static async register(data: any): Promise<void> {
        const value = await serviceSchema.createService.validate(data)
        const alreadyService = await database.service.findFirst({
            where: { name: value.name }
        })
        let serviceId
        if (!alreadyService) {
            const newService = await database.service.create({
                data: {
                    name: value.name,
                    description: value.description
                }
            })
            serviceId = newService.id
        } else {
            serviceId = alreadyService.id
        }

        const alreadyInstance = await database.serviceInstance.findFirst({
            where: {
                instanceName: value.instanceName
            }
        })
        let serviceInstance
        if (alreadyInstance) {
            serviceInstance = await database.serviceInstance.update({
                where: { id: alreadyInstance.id },
                data: {
                    address: value.address,
                    version: value.version,
                    topicPrefix: value.topicPrefix,
                }
            })
        } else {
            serviceInstance = await database.serviceInstance.create({
                data: {
                    serviceId,
                    address: value.address,
                    instanceName: value.instanceName,
                    isOnlineTopic: value.isOnlineTopic,
                    version: value.version,
                    topicPrefix: value.topicPrefix
                }
            })
        }
        console.log(`[Successfully registered service]`, serviceInstance);
    }

    public static async getServices(data: any): Promise<any> {
        const value = await serviceSchema.getServices.validate(data)
        let res: any[]
        if (value.name) {
            const service = await database.service.findFirst({
                where: { name: value.name }
            })
            if (!service) {
                res = []
            } else {
                res = await database.serviceInstance.findMany({
                    where: {
                        serviceId: service.id
                    },
                    include: { service: true }
                })
            }
        } else {
            res = await database.serviceInstance.findMany({
                where: {
                    instanceName: value.instanceName,
                    address: value.address
                },
                include: { service: true }
            })
        }

        console.log(`[Successfully fount services]`, res);
        return res
    }
}