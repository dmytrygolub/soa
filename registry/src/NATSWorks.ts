import {Client, connect, Msg, NatsConnectionOptions} from "ts-nats";

class NATSPublishers {
    opts = {} as NatsConnectionOptions;
    nc: Client
    constructor(url: string = 'localhost:4222') {
        this.opts.url = url;
    }

    async init(client: Client | undefined = undefined): Promise<void> {
        if (!client)
            this.nc = await connect(this.opts);
        else
            this.nc = client
        this.nc.on('permissionError', (err) => {
            console.log(`${err}`);
        });
    }

    async makeRequest(subject: string, data: any, max: number = -1): Promise<any | undefined> {
        max = parseInt(max.toString(), 10);
        if (max < 1) {
            max = 1000;
        }
        const payload = JSON.stringify(data)
        const message = await this.nc.request(subject, max, payload);
        if (!message.data)
            return undefined;
        return JSON.parse(message.data)
    }

    async publish(subject: string, data: any): Promise<any> {
        const payload = JSON.stringify(data)
        this.nc.publish(subject, payload);
        await this.nc.flush()
    }
}

class NATSController {
    opts = {} as NatsConnectionOptions;
    nc: Client
    constructor(url: string = 'localhost:4222') {
        this.opts.url = url;
    }

    getTopic(): string {
        return 'test'
    }

    async init(client: Client | undefined = undefined): Promise<void> {
        if (!client)
            this.nc = await connect(this.opts);
        else
            this.nc = client
        this.nc.on('permissionError', (err) => {
            console.log(`${err}`);
        });
        await this.nc.subscribe(this.getTopic(), async (err, msg) => {
            if (err) {
                console.error(`[] error processing message [${err.message} - ${msg}`);
                return;
            }
            if (msg) {
                const msgData = JSON.parse(msg.data)
                try {
                    await this.onMessage(msgData)
                } catch (e) {
                    console.error(`Error on ${this.getTopic()}`, e)
                }
            }
        });
    }

    async onMessage(msg: any): Promise<void> {
        console.error(`Got message ${msg}`);
    }
}

class NATSReplyController {
    opts = {} as NatsConnectionOptions;
    nc: Client
    constructor(url: string = 'localhost:4222') {
        this.opts.url = url;
    }

    getTopic(): string {
        return 'test'
    }

    async init(client: Client | undefined = undefined): Promise<void> {
        if (!client)
            this.nc = await connect(this.opts);
        else
            this.nc = client
        this.nc.on('permissionError', (err) => {
            console.log(`${err}`);
        });

        this.nc.subscribe(this.getTopic(), async (err, msg) => {
            if (err) {
                console.error(`[] error processing message [${err.message} - ${msg}`);
                return;
            }
            if (msg && msg.reply) {
                try {
                    const msgData = JSON.parse(msg.data)
                    const response = await this.onMessage(msgData)
                    const payload = JSON.stringify(response)
                    this.nc.publish(msg.reply, payload)
                } catch (e) {
                    console.error(`Error on ${this.getTopic()}`, e)
                }
            }
        });
    }

    async onMessage(msg: any): Promise<any> {
        console.error(`Got message ${msg.data}`);
        return {}
    }
}

export { NATSPublishers, NATSController, NATSReplyController }