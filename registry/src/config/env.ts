import 'dotenv/config'

function getEnv(value: any, fallback?: any): any {
    const result = process.env[value]

    // check env value
    if ([undefined, null, ''].includes(result)) {
        // check fallback
        if (fallback) {
            return fallback
        }

        return undefined
    }

    return result
}

const appEnv = {
    NATS_URL: getEnv('NATS_URL', 'localhost:4222'),
}

export {
    appEnv
}