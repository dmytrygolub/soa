import * as Yup from 'yup';

const createService = Yup.object().shape({
    name: Yup.string().required(),
    description: Yup.string(),
    address: Yup.string().required(),
    instanceName: Yup.string().required(),
    isOnlineTopic: Yup.string(),
    version: Yup.string(),
    topicPrefix: Yup.string(),
});

const getServices = Yup.object().shape({
    name: Yup.string(),
    instanceName: Yup.string(),
    address: Yup.string(),
});


export const serviceSchema = {
    createService,
    getServices
}