import {NATSPublishers} from "~/NATSWorks";
import {Client, connect, NatsConnectionOptions} from "ts-nats";

let opts = {} as NatsConnectionOptions;
opts.url = 'localhost:4222'

let publisher: NATSPublishers
let client: Client

async function init() {
    client = await connect(opts);

    client.on('permissionError', (err) => {
        client.close();
        console.log(`${err}`);
    });

    publisher = new NATSPublishers();
    await publisher.init(client)
}

export { publisher, client, init }
