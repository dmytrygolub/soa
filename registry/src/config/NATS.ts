import {NATSPublishers} from "~/NATSWorks";
import {Client, connect, NatsConnectionOptions} from "ts-nats";
import {appEnv} from "~/config/env";

let opts = {} as NatsConnectionOptions;
opts.url = appEnv.NATS_URL

let publisher: NATSPublishers
let client: Client

async function init() {
    client = await connect(opts);

    client.on('permissionError', (err) => {
        client.close();
        console.log(`${err}`);
    });

    publisher = new NATSPublishers();
    await publisher.init(client)
}

export {publisher, client, init}
