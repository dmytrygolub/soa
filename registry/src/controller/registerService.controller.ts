import {NATSController} from "~/NATSWorks";
import RegisterServiceService from "~/service/registerServices.service";
export default class RegisterServiceController extends NATSController {
    override getTopic(): string {
        return 'register-new-service';
    }

    override async onMessage(msg: any): Promise<any> {
        console.log(`[Got register message]`, msg);
        await RegisterServiceService.register(msg)
    }
}