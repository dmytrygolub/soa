import {NATSController, NATSReplyController} from "~/NATSWorks";
import RegisterServiceService from "~/service/registerServices.service";
export default class GetServicesController extends NATSReplyController {
    override getTopic(): string {
        return 'get-services';
    }

    override async onMessage(msg: any): Promise<any> {
        console.log(`[Got get message]`, msg);
        return await RegisterServiceService.getServices(msg)
    }
}