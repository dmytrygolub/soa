import PingController from "~/controller/ping.controller";
import {client as NATSClient, init as NATSInit, publisher as NATSPublisher} from "~/config/NATS";

const controllers = [new PingController()]

async function main() {
    await NATSInit()

    for (const controller of controllers) {
        await controller.init(NATSClient)
    }

    await NATSPublisher.publish('register-new-service', {
        name: 'simple-service',
        description: 'Demonstrate abilities of registry service',
        address: 'localhost:4222',
        instanceName: 'simple-service-dev',
        isOnlineTopic: 'simple-service-ping-topic',
        version: 'dev 1.1.0',
        topicPrefix: 'simple-service'
    })
}

main();