import {database} from "~/config/database";
import cron from 'node-cron';
import {publisher as NATSPublisher} from "~/config/NATS";

export default class IsOnlinePingService {
    public static init() {
        // Schedule the isOnline method to run every minute (at the 0th second of each minute)
        cron.schedule('* * * * *', IsOnlinePingService.isOnline);
    }
    public static async isOnline(): Promise<void> {
        const servicesToCheck = await database.serviceInstance.findMany({
            where: { NOT: { isOnlineTopic: null } }
        })
        console.log(`[Got services to check] ${servicesToCheck.map((s) => s.instanceName)}`)
        const results = await Promise.all(servicesToCheck.map(async (s) => {
            try {
                const res = await NATSPublisher.makeRequest(s.isOnlineTopic!!, {}, 1000)
                if (res) {
                    return { id: s.id, isOnline: true }
                } else {
                    throw new Error('Service isn\'t responding')
                }
            } catch (e) {
                return { id: s.id, isOnline: false }
            }
        }))
        console.log(`[Services checked] ${results.map((r) => `id: ${r.id}, isOnline: ${r.isOnline}`)}`)
        for (const res of results) {
            await database.serviceInstance.update({
                where: { id: res.id },
                data: { isOnline: res.isOnline }
            })
        }
        console.log(`[Successfully checked online]`);
    }
}