import {NATSReplyController} from "~/NATSWorks";
export default class PingController extends NATSReplyController {
    override getTopic(): string {
        return 'simple-service-ping-topic';
    }

    override async onMessage(msg: any): Promise<any> {
        console.log(`[Got ping message]`);
        return {}
    }
}