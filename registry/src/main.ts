import RegisterServiceController from "~/controller/registerService.controller";
import GetServicesController from "~/controller/getServices.controller";
import IsOnlinePingService from "~/service/isOnlinePing.service";
import {client as NATSClient, init as NATSInit} from "~/config/NATS";

const controllers = [new RegisterServiceController(), new GetServicesController()]

async function main() {
    await NATSInit()
    IsOnlinePingService.init()

    for (const controller of controllers) {
        await controller.init(NATSClient)
    }
}

main();